import java.util.Stack;
import java.util.Vector;

public class Main {

/*    Stack este o clasa copil a clasei Vector care si ea implementeaza AbstactList - List - Collection si in final Iterable
    Stack sau stiva, este implementarea in java a principiului FILO ( first in, last out)*/

    public static void main(String[] args) {

        Stack<Integer> test = new Stack<>();

/*
        Ca si metode avem :

        1. push - pentru a intoduce obiecte in stiva noastra;
        Nu am mai creat inca o clasa pentru a genera obiecte si a le introduce in stiva. Am utilizat primitive de tip int
        pentru ca java face autoboxing si converteste primitiva in wrapper class ei, si anume Integer
*/

        for (int i = 1; i <= 10; i++) {
            test.push(i);
        }
        System.out.println("Dimensiunea stivei nou create este de " + test.size() + " elemente ");

        System.out.println("Obiectul '9' se afla la pozitia index " + test.search(9) + " pentru ca este penultimul introdus in stiva si al doilea in cauzul " +
                "utilizarii metodei pop sau peek");

        /*        Si uite asa avem o stiva cu 10 elemente*/

        /*2. peek - ne returneaza ultimul element din stiva, in cazul nostru fiint obiectul 9, facut unboxing la primitiva sa*/

        System.out.println("Utimul element introdus in stiva este " + test.peek());

        /*3. pop - pe acelasi principiu avem si metoda pop, care elimina din stiva nostra ultimul element introdus, pe care il si returneaza*/

        System.out.println();
        System.out.println("Ultimul element introdus in stiva noastra este tot " + test.pop() + " insa dupa utilizarea metodei pop, stiva are dimensiunea de " + test.size());

/*        Si uite asa putem goli stiva, element cu element*/

        while (!test.empty()){
            System.out.println("Am eliminat si elementul " + test.pop());
        }
        System.out.println("Si acum stima este goala ");

        /*Ca si particularitate, la fel ca si clasa parinte, Stack este synchronized*/
    }
}
